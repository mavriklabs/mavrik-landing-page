/*******FAQ*******/
jQuery(document).ready(function($) {
  $('.question').on('click', function() {
    if ($(this).hasClass('active')) {
      $('.question').removeClass('active');
      $('.shortText').removeClass('shortTextNone');
      $('.arrowImg').removeClass('arrow-active');
      $('.answer').slideUp(200);
    } else {

      $('.question').removeClass('active');
      $('.arrowImg').removeClass('arrow-active');
      $('.shortText').removeClass('shortTextNone');
      $('.answer').slideUp();
      $(this).addClass('active');
      $(this).find('.shortText').addClass('shortTextNone');
      $(this).find('.arrowImg').addClass('arrow-active');
      $(this).children('.answer').slideToggle(200);
    }
  });
});
/*******MOBILE NAVIGATION********/
$(document).ready(function() {
  $('.u-nav').on('click', function() {
    $(this).addClass('is-open');
    $('.u-nav-container').addClass('is-open');
    if ($('.u-nav').hasClass('is-open')) { //enable-disable scroll
      window.onscroll = function() {
        window.scrollTo(0, 0);
      };
    } else {
      window.onscroll = function() {};
    }
  });

  $('.closeNav').on('click', function() {
    $('.u-nav').removeClass('is-open');
    $('.u-nav-container').removeClass('is-open');
    if ($('.u-nav').hasClass('is-open')) { //enable-disable scroll
      window.onscroll = function() {
        window.scrollTo(0, 0);
      };
    } else {
      window.onscroll = function() {};
    }
  });

});

/*******PRELOADER******/
setTimeout(function() {
  $('#preloader').fadeOut('fast', function() {
    $(this).remove();
  });
}, 1000);



/*********WHEN USER CLICK ON LINK IN NAV BAR PAGE WILL SCROLLED TO CLICKED TARGET*********/
$('#wrap a[href]').click(function() {
  $('.u-nav-container').removeClass('is-open');
  $('.u-nav').removeClass('is-open');
  window.onscroll = function() {};
  var targetHref = $(this).attr('href');
  console.log(targetHref);
  $('html, body').animate({
    scrollTop: $(targetHref).offset().top
  }, 1000);
});
